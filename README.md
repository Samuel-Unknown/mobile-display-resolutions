# Репозиторий с шаблонами наиболее популярных разрешений экранов для рисования мобильной 2D игры #

Подробнее читай тут -> [https://goo.gl/qZb7Gt](https://goo.gl/qZb7Gt)

![Top-Mobile-Display-Resolution.png](https://bitbucket.org/repo/LqGBLq/images/3654828945-Top-Mobile-Display-Resolution.png)

![Scaled-Resolutions.png](https://bitbucket.org/repo/LqGBLq/images/3975035603-Scaled-Resolutions.png)

![Mobile-Scaled-Resolutions-Vertical.png](https://bitbucket.org/repo/LqGBLq/images/2273495280-Mobile-Scaled-Resolutions-Vertical.png)